
import java.util.*;

public class TreeNode {

	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	TreeNode(String n, TreeNode d, TreeNode r) {
		setName(n);
		setFirstChild(d);
		setNextSibling(r);

	}

	public void setName(String n) {
		name = n;
	}

	public String getName() {
		return name;
	}

	public void setFirstChild(TreeNode d) {
		firstChild = d;
	}

	public TreeNode getFirstChild() {
		return firstChild;
	}

	public void setNextSibling(TreeNode r) {
		nextSibling = r;
	}

	public TreeNode getNextSibling() {
		return nextSibling;
	}

	/**
	 * Meetod kontrollib esmalt etteantava stringi korrektsust ja seejärel
	 * moodustab stringi baasil puu
	 * 
	 * @param s
	 *            on ettantav string
	 * @return korrektse stringi korral tagastab etteantud stringi s; vigase
	 *         stringi korral annab veateate
	 * 
	 *         koodi aluseks olevad materjalid: https://codetidy.com/3910
	 *         http://enos.itcollege.ee/~ylari/I231/Node.java
	 */

	public static TreeNode parsePrefix(String s) {

		TreeNode t = null;

		// Kas sulud on paaris

		int pikkus = s.length();
		int counterAlgavSulg = 0;
		int counterLoppevSulg = 0;

		for (int i = 0; i < pikkus; i++) {
			if (s.charAt(i) == '(') {
				counterAlgavSulg++;
			}
		}

		for (int i = 0; i < pikkus; i++) {
			if (s.charAt(i) == ')') {
				counterLoppevSulg++;
			}
		}

		if (counterAlgavSulg != counterLoppevSulg) {
			throw new RuntimeException("Sisestatud avaldises " + s + " on sulud valesti!");
		}

		// Tipu asemel tühik
		if (s.equals("\t")) {
			throw new RuntimeException("Sisestatud on tühi string!");
		}

		// Tipu nimes tühik
		if (!s.contains("(") && !s.contains(",") && !s.contains(")") && s.contains(" ")) {
			throw new RuntimeException("Sisestatud avaldises " + s + " on tipu nimes tühik!");
		}

		// Ilma sulgudeta
		if (!s.contains("(") && s.contains(",")) {
			throw new RuntimeException("Sisestatud avaldises " + s + " ei ole sulge!");
		}

		// Topeltsulgudega
		for (int i = 0; i < pikkus; i++) {
			if (s.charAt(i) == '(' && s.charAt(i + 1) == '(') {
				throw new RuntimeException("Sisestatud avaldises " + s + " on topeltsulud!");
			}

		}

		// Komade testid

		for (int i = 0; i < pikkus; i++) {
			if (s.charAt(i) == ',' && s.charAt(i + 1) == ',') {// kaks koma
				throw new RuntimeException("Sisestatud avaldises " + s + " on järjest kaks koma!");
			}

			if (s.charAt(i) == '(' && s.charAt(i + 1) == ',') {// avavale sulule
																// järgneb koma
				throw new RuntimeException(
						"Sisestatud avaldis " + s + " ei ole korrektne - avavale sulule järgneb koma!");
			}
		}

		// ("A(B),C(D)");//ei saa olla rohkem kui üks juur ja koma ei tohi
		// tulla pärast juurele järgneva sulu kinniminekut

		int i = 0;
		int countSulg = 0;
		int pikkus1 = s.length();
		int suluLopuIndeks = 0;

		int countKokkuSulud = 0;
		// System.out.println(pikkus1);

		for (i = 0; i < s.length(); i++) {

			if (s.charAt(i) == '(') {
				countSulg++;
				countKokkuSulud++;

				// System.out.println(countSulg);
			}

			if (s.charAt(i) == ')') {
				countSulg--;
				countKokkuSulud++;
			}

			// System.out.println(countSulg);}

			if (countKokkuSulud > 1 && countSulg == 0) {
				suluLopuIndeks = (i);
				// System.out.println(suluLopuIndeks);
				// System.out.println(s.charAt(i));
				// System.out.println(s.length());
				if (pikkus1 > (suluLopuIndeks) + 2) {
					;// kui string jätkub pärast juurele lapse sulge
						// if(s.charAt(i)==','){
					throw new RuntimeException("Sisestatud avaldis " + s + " ei ole korrektne - ...");
				}
				;
			}
			;
		}

		t = parsimine(s);
		return t;
	}

	public static TreeNode parsimine(String s) {
		TreeNode t = null;
		String juur = "";
		String firstChild = "";
		String nextSibling = "";
		String element;
		int sulgudeCounter = 0;

		StringTokenizer st = new StringTokenizer(s, "(),", true);
		if (st.countTokens() == 1) {
			juur = st.nextToken().trim();
			t = new TreeNode(juur, null, null);// kui puus on ainult juur

		}

		if (st.countTokens() > 1) {// kui puus on juur ning üks või rohkem tippe
			juur = st.nextToken().trim();
			while (st.hasMoreTokens()) {// kui juurtipule järgneb midagi
				element = st.nextToken().trim();// element salvestatakse
												// muutujasse ja trim
												// eemaldab seejuures
												// tühikud //
												// mittevajalikud
												// tühikud

				if (element.equals("(")) {// kui see element on avav sulg, siis
											// on selle sulu sees olev suluesise
											// tipu esimene laps
					sulgudeCounter++;// loetakse sulg

					element = st.nextToken().trim(); // avavale sulule
					// järgnev element

					while (sulgudeCounter > 0) {// kui ollakse sulgude sees
						firstChild = firstChild + element;
						element = st.nextToken().trim(); // järgnev element

						if (element.equals("(")) {// kui tuleb uus algav sulg,
													// siis selle loeme
													// loenduriga, aga nö astume
													// mööda
							sulgudeCounter++;

						}
						;

						if (element.equals(")")) {// kui tuleb sulgev sulg,
													// siis selle loeme
													// loenduriga ja astume
													// edasi
							sulgudeCounter--;

						}
						;

					} // while
				} // if"("

				if (element.equals(",")) {// kui element on koma, siis on kogu
											// sellele järgnev elementide rida
											// koma ees oleva tipu parem naaber)
					while (st.hasMoreTokens()) {
						element = st.nextToken().trim();
						nextSibling = nextSibling + element;
					} // while
				} // if ","
					// System.out.println(element);

			} // while

			t = new TreeNode(juur, TreeNode.parsimine(firstChild), TreeNode.parsimine(nextSibling));// moodusta
																									// puu

		}

		return t; // return the root}
	}

	public String rightParentheticRepresentation() {
		StringBuffer b = new StringBuffer();
		TreeNode tipp = firstChild;

		if (firstChild != null)
			b.append("(");
		while (tipp != null) {
			b.append(tipp.rightParentheticRepresentation());
			tipp = tipp.nextSibling;
			if (tipp != null)
				b.append(",");
		}
		if (firstChild != null)
			b.append(")");
		// b.append(root);
		return b.append(name).toString();
	}

	public static void main(String[] param) {

		String s = "A(B1,C,D)";
		// String s = "ABC(E,D(F))";
		// String s = "A(B),C(D)";

		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
	}
}
